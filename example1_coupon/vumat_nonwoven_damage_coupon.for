!*********************************************************************
! VUMAT subroutine for the damage model of fiber network materials
!
! Bond damage is determined by solving local force balance equations.
! Fitted damage evolution law (6 parameters) is used.
!     
! Non-local averaging scheme is incorporated. The stretch ratio of each
! fiber set at one element is averaged by the surrounding elements.
! Non-local weights are calculated at the beginning of the simulation.
! Use a sparse matrix to store non-local weights.
!
! Chen, N., & Silberstein, M. N. (2019). A micromechanics-based damage model 
! for non-woven fiber networks. International Journal of Solids and Structures, 
! 160, 18-31.
!
!*********************************************************************

!*********************************************************************
      MODULE MATPARAM
! This module stores all the material properties. The sequence is:
! First line: network parameters nuf, numf
! Second line: fiber model parameters Ef, e0_dot, s0, n, h, ssat
! Third line: damage evolution law parameters: a1, a2, a3, a4, a5, a6
      
! Damage evolution law parameters for the following cases are written here
! SF20, SF32(gradual), SF32(rupture), SF65, no damage
      
      REAL*8, PARAMETER ::       matprop(14) = [1.,-1.,
     +           2400.,1.e-18,1.7336,4.9603,41.5508,10.6770,
     +           0.1695,1.8530,0.3102,1.8876,1.0746,1.2395]
     +           !0.1791,1.8346,0.3104,1.9868,1.1185,1.6369] 
     +           !0.5118,4.5715,0.6146,1.5116,0.3137,2.2659]
     +           !0.5235,5.2270,0.4617,1.5352,0.5593,61.4595]
     +           !1., 0., 1., 0., 1., 0.]
	  
      
      END MODULE MATPARAM 
!*********************************************************************
      
!*********************************************************************
      MODULE ELEMGBL
! This module stores global state variables information
      
      
      ! Number of fiber sets
      INTEGER, PARAMETER :: numf=20
      
      !-----------------------------------------------------------
      ! Global information used for non-local averaging
      
      ! Number of elements in x and y directions. And total number of elements
      INTEGER, PARAMETER :: gblnumx=60, gblnumy=20, gblnumelem=1200
      
      ! Non-local interaction distance
      REAL*8, PARAMETER :: lch=20.1
      
      ! nllim (the second dimension in sparse matrix representation nlnz)
      ! nllim should be .GE. (2*lch+1)^2
      INTEGER, PARAMETER :: nllim=1200

      ! Local (gblsr1) and non-local averaged (gblsr2) fiber set stretch ratios
      REAL*8 :: gblsr1(gblnumelem,numf)
      REAL*8 :: gblsr2(gblnumelem,numf)
      
      ! Non-local weights
      REAL*8 :: nlwi(gblnumelem,gblnumelem)
      
      ! Fiber volume fraction information
      ! It is used to define a ``defect''
      REAL*8 :: mat_nuf(gblnumelem)
	
	! Non-zero component information in nlwi (nlwi is sparse)
      INTEGER :: nlnz(gblnumelem,nllim)
      
      !-----------------------------------------------------------
      ! Local state variable information
      
      ! For each fiber set, there are five state variables.
      ! The sequence is: plastic stretch, shear resistance 1, 
      ! shear resistance 2, damage, total stretch
      REAL*8 :: statev(5,numf)
      

      END MODULE ELEMGBL    
!*********************************************************************

!*********************************************************************
      SUBROUTINE vumat(
     +     jblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     +     stepTime, totalTime, dt, cmname, coordMp, charLength,
     +     props, density, strainInc, relSpinInc,
     +     tempOld, stretchOld, defgradOld, fieldOld,
     +     stressOld, stateOld, enerInternOld, enerInelasOld,
     +     tempNew, stretchNew, defgradNew, fieldNew,
     +     stressNew, stateNew, enerInternNew, enerInelasNew)
      
      INCLUDE 'vaba_param.inc'

      DIMENSION jblock(*), props(nprops), density(*), 
     +     coordMp(*),
     +     charLength(*), strainInc(*),
     +     relSpinInc(*), tempOld(*),
     +     stretchOld(*),
     +     defgradOld(*),
     +     fieldOld(*), stressOld(*),
     +     stateOld(*), enerInternOld(*),
     +     enerInelasOld(*), tempNew(*),
     +     stretchNew(*),
     +     defgradNew(*),
     +     fieldNew(*),
     +     stressNew(*), stateNew(*),
     +     enerInternNew(*), enerInelasNew(*)

      CHARACTER*80 cmname
      
      PARAMETER (
     +    i_umt_nblock=1,
     +    i_umt_npt=2,
     +    i_umt_layer=3,
     +    i_umt_kspt=4,
     +    i_umt_noel=5)
          
      CALL vumatXtrArg(jblock(i_umt_nblock),
     +     ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     +     stepTime, totalTime, dt, cmname, coordMp, charLength,
     +     props, density, strainInc, relSpinInc,
     +     tempOld, stretchOld, defgradOld, fieldOld,
     +     stressOld, stateOld, enerInternOld, enerInelasOld,
     +     tempNew, stretchNew, defgradNew, fieldNew,
     +     stressNew, stateNew, enerInternNew, enerInelasNew,
     +     jblock(i_umt_noel),jblock(i_umt_npt),
     +     jblock(i_umt_layer),jblock(i_umt_kspt))


      RETURN
      END SUBROUTINE vumat
!*********************************************************************
      
!*********************************************************************
      SUBROUTINE vumatXtrArg(
! An interface vumat subroutine to get global element number
! During each call of vumat, only 136 number of material points are
! computed for stress contribution
     +     nblock, ndir, nshr, nstatev, nfieldv, nprops, lanneal,
     +     stepTime, totalTime, dt, cmname, coordMp, charLength,
     +     props, density, strainInc, relSpinInc,
     +     tempOld, stretchOld, defgradOld, fieldOld,
     +     stressOld, stateOld, enerInternOld, enerInelasOld,
     +     tempNew, stretchNew, defgradNew, fieldNew,
     +     stressNew, stateNew, enerInternNew, enerInelasNew,
     +     nElement,nMatPoint,nLayer,nSecPoint)
      
      ! Import modulues
      USE MATPARAM
      USE ELEMGBL
        
      INCLUDE 'vaba_param.inc'

      DIMENSION props(nprops), density(nblock), coordMp(nblock,*),
     +     charLength(nblock), strainInc(nblock,ndir+nshr),
     +     relSpinInc(nblock,nshr), tempOld(nblock),
     +     stretchOld(nblock,ndir+nshr),
     +     defgradOld(nblock,ndir+nshr+nshr),
     +     fieldOld(nblock,nfieldv), stressOld(nblock,ndir+nshr),
     +     stateOld(nblock,nstatev), enerInternOld(nblock),
     +     enerInelasOld(nblock), tempNew(nblock),
     +     stretchNew(nblock,ndir+nshr),
     +     defgradNew(nblock,ndir+nshr+nshr),
     +     fieldNew(nblock,nfieldv),
     +     stressNew(nblock,ndir+nshr), stateNew(nblock,nstatev),
     +     enerInternNew(nblock), enerInelasNew(nblock)
      
      ! Documentation of extra arguments:
      !  nElement: Array of internal element numbers
      DIMENSION nElement(nblock)
      !  nMatPoint: Integration point number
      !  nLayer   : Layer number for composite shells and layered solids
      !  nSecPoint: Section point number within the current layer

      CHARACTER*80 cmname

      REAL*8 I_1(3,3),F_t(3,3),F_tau(3,3),U_t(3,3),U_tau(3,3)
      REAL*8 T_tau(3,3),R_tau(3,3),U_inv(3,3),detF

	!-----------------------------------------------------------
      ! Declare variables 
      ! The variables below are mainly used for calculating non-local weights
      
      ! Coordinate information of all the elements
      REAL*8 :: gblcoord(gblnumelem,2)
      
      ! Distances from all the elements to the element of interest (local element)
      REAL*8 :: dist(gblnumelem)
      
      ! Temporary weights
      ! crt is used when there's a seam in the specimen domain
      REAL*8 :: tpwi(gblnumelem),crt
      
      ! Dummy variables for loops
      INTEGER :: nli,nlj,nlii,nljj,nlkk,nlll
      
      ! celemnum is the global element number corresponds to the current material point
      INTEGER :: celemnum

      !-----------------------------------------------------------
      ! Calculate non-local weights
      ! This setting is only applicable to CPS4R elements in ABAQUS/Explicit
      ! One material point is assigned to each element. If a different element
      ! type is used, the non-local distance and weight calculation should be changed
      
      ! This step is done at the beginning of a simulation
      IF(totalTime .LE. dt) THEN
          
          ! Element coordinate calculation (in undeformed configuration)
          ! May need to change when a non-rectangular specimen geometry is used
          DO nlj=1,gblnumy
              DO nli=1,gblnumx
                  gblcoord((nlj-1)*gblnumx+nli,1) = 0.5+(nli-1)*1.
                  gblcoord((nlj-1)*gblnumx+nli,2) = 0.5+(nlj-1)*1.
              END DO
          END DO
      
          ! Initialize non-local weights nlwi and sparse matrix representation nlnz
          nlwi = 0.
          nlnz = 0
          
          ! Loop over all the elements of interests
          DO nlii = 1,gblnumelem
              
              ! Some ``defect'' elements have different volume fractions
              IF ((gblcoord(nlii,1) .EQ. 29.5) .AND. 
     +            (gblcoord(nlii,2) .EQ. 9.5)) THEN
                   mat_nuf(nlii) = 0.9
              ELSE
                   mat_nuf(nlii) = 1.
              END IF
          
              ! nlkk is related to the second dimension in nlnz
              nlkk = 1
              
              ! Temporary matrix to store weights information
              tpwi = 0.
              
              ! Loop over all the elements 
              ! Calculate element to element distance and non-local weights
              DO nljj = 1,gblnumelem

                  dist(nljj) = SQRT((gblcoord(nljj,1)-
     +                           gblcoord(nlii,1))**2.+
     +                (gblcoord(nljj,2)-gblcoord(nlii,2))**2.)
                  
                  ! Unscaled weight function
                  !CALL seam_geo(gblcoord(nlii,:),gblcoord(nljj,:),crt)
                  !IF ((dist(nljj) .LE. lch) .AND. (crt .NE. 1.)) THEN
                  IF (dist(nljj) .LE. lch) THEN
                      tpwi(nljj) = EXP(-(2.*dist(nljj)/lch)**2.)
                      nlnz(nlii,nlkk) = nljj
                      nlkk = nlkk+1
                  ELSE
                      tpwi(nljj) = 0.
                  END IF
                  
              END DO  
              ! Scale all the weights of one element so their sum is 1
              nlwi(nlii,:) = tpwi/SUM(tpwi)
          END DO
          
      END IF
      
      !-----------------------------------------------------------
      ! Loop over all the material points
      DO km = 1,nblock
         
         ! Global element number of current material point
         celemnum = nElement(km) 
          
         ! Copy old and new deformation gradients
         F_t(1,1) = defgradOld(km,1)
         F_t(2,2) = defgradOld(km,2)
         F_t(3,3) = defgradOld(km,3)
         F_t(1,2) = defgradOld(km,4)
         F_tau(1,1) = defgradNew(km,1)
         F_tau(2,2) = defgradNew(km,2)
         F_tau(3,3) = defgradNew(km,3)
         F_tau(1,2) = defgradNew(km,4)
         U_tau(1,1) = stretchNew(km,1)
         U_tau(2,2) = stretchNew(km,2)
         U_tau(3,3) = stretchNew(km,3)
         U_tau(1,2) = stretchNew(km,4)
         IF(nshr .lt. 2) THEN
            ! 2D case
            F_t(2,1) = defgradOld(km,5)
            F_t(1,3) = 0.d0
            F_t(2,3) = 0.d0
            F_t(3,1) = 0.d0
            F_t(3,2) = 0.d0
            F_tau(2,1) = defgradNew(km,5)
            F_tau(1,3) = 0.d0
            F_tau(2,3) = 0.d0
            F_tau(3,1) = 0.d0
            F_tau(3,2) = 0.d0
            U_tau(2,1) = U_tau(1,2)
            U_tau(1,3) = 0.d0
            U_tau(2,3) = 0.d0
            U_tau(3,1) = 0.d0
            U_tau(3,2) = 0.d0
         ELSE
            ! 3D case
            WRITE(*,*) '3D'
         END IF 
          
         !-----------------------------------------------------------
         ! Calculate stress response of current material point
         ! Update state variables
         IF((totalTime.eq.0.d0).and.(stepTime.eq.0.d0)) THEN
             
             ! Initalize state variables
             ! Total number of state variables used in ABAQUS is numf*5+2
             stateOld(km,1:numf) = 1.     ! plastic stretch of each fiber set
             stateOld(km,numf+1:2*numf) = REAL(matprop(5))    ! shear resistance 1 of each fiber set
             stateOld(km,2*numf+1:3*numf) = 0.    ! shear resistance 2 of each fiber set
             stateOld(km,3*numf+1:4*numf) = 0.    ! damage of each fiber set
             stateOld(km,4*numf+1:5*numf) = 1.    ! total stretch of each fiber set
             stateOld(km,5*numf+1) = 0.   ! average damage of this material point
             stateOld(km,5*numf+2) = 1.   ! deletion flag of this material point
             
             ! Local state variables
             statev(1,1:numf) = stateOld(km,1:numf)
             statev(2,1:numf) = stateOld(km,numf+1:2*numf)
             statev(3,1:numf) = stateOld(km,2*numf+1:3*numf)
             statev(4,1:numf) = stateOld(km,3*numf+1:4*numf)
             statev(5,1:numf) = stateOld(km,4*numf+1:5*numf)
             
             ! Initialize local and non-local averaged fiber stretches
             gblsr1(celemnum,1:numf) = 1.
             gblsr2(celemnum,1:numf) = 1.

             ! Calculate stress response
             CALL network_stress(F_tau,T_tau,dt,mat_nuf(celemnum))

         ELSE
             
             ! Read old state variables (local stretch and damage)
             ! Damage doesn't have non-local counterpart. Fiber stretch does
             statev(1,1:numf) = stateOld(km,1:numf)
             statev(2,1:numf) = stateOld(km,numf+1:2*numf)
             statev(3,1:numf) = stateOld(km,2*numf+1:3*numf)
             statev(4,1:numf) = stateOld(km,3*numf+1:4*numf)
             statev(5,1:numf) = stateOld(km,4*numf+1:5*numf)
             
             ! Update local fiber stretches
             CALL network_stretch(F_tau,celemnum)

             ! Perform non-local averaging step
             gblsr2(celemnum,1:numf) = 0.
             ! Only use the nonzero component in nlwi
             ! We should call nlnz and see what the nonzero components are
             DO nlkk = 1,nllim
                 nljj = nlnz(celemnum,nlkk) 
                 IF (nljj .GE. 1) THEN
                     ! Put all damage variables together
                     gblsr2(celemnum,1:numf) = 
     +                  gblsr2(celemnum,1:numf)+
     +                  nlwi(celemnum,nljj)*gblsr1(nljj,1:numf)
                 END IF             
             END DO 
             statev(5,1:numf) = gblsr2(celemnum,1:numf)
                 
             ! Update damage variables, using loading-unloading conditions
             CALL network_damage(celemnum)    

             ! Calculate stress response
             CALL network_stress(F_tau,T_tau,dt,mat_nuf(celemnum))
             
         END IF

         ! Update state variables used in ABAQUS
         stateNew(km,1:numf) = statev(1,1:numf) 
         stateNew(km,numf+1:2*numf) = statev(2,1:numf) 
         stateNew(km,2*numf+1:3*numf) = statev(3,1:numf) 
         stateNew(km,3*numf+1:4*numf) = statev(4,1:numf)
         stateNew(km,4*numf+1:5*numf) = statev(5,1:numf)
         stateNew(km,5*numf+1) = SUM(statev(4,:))/REAL(numf)
         ! Use local damage information as the element deletion criterion
         IF (stateNew(km,5*numf+1)  .LE. 0.999) THEN
             stateNew(km,5*numf+2) = 1.
         ELSE
             stateNew(km,5*numf+2) = 0.
         END IF
         

         !-----------------------------------------------------------
         ! ABAQUS/Explicit uses stress measure (transpose(R) T R)
         
         CALL m3inv(U_tau,U_inv)
         R_tau = MATMUL(F_tau,U_inv)
         T_tau = MATMUL(TRANSPOSE(R_tau),MATMUL(T_tau,R_tau))

         DO i=1,ndir
            stressNew(km,i) = T_tau(i,i)
         END DO
         IF(nshr.ne.0) THEN
            stressNew(km,ndir+1) = T_tau(1,2)
            IF(nshr.ne.1) THEN
               stressNew(km, ndir+2) = T_tau(2,3)
               IF(nshr.ne.2) THEN
                  stressNew(km,ndir+3) = T_tau(1,3)
               END IF
            END IF
         END IF

         ! Update the specific internal energy
         stress_power = 0.d0
         DO i = 1,ndir
            stress_power = stress_power +
     +           0.5*((StressOld(km,i)+StressNew(km,i))*
     +           StrainInc(km,i))
         END DO
         
         SELECT CASE (nshr)
         CASE(1)
            stress_power = stress_power + 
     +           0.5*((StressOld(km,ndir+1)+StressNew(km,ndir+1))*
     +           StrainInc(km,ndir+1))
         CASE(3)
            stress_power = stress_power + 
     +           0.5*(((StressOld(km,ndir+1) + StressNew(km,ndir+1))*
     +           StrainInc(km,ndir+1)) +
     +           ((StressOld(km,ndir+2)+ StressNew(km,ndir+2)) *
     +           StrainInc(km,ndir+2))+
     +           ((StressOld(km,ndir+3) + StressNew(km,ndir+3))*
     +           StrainInc(km,ndir+3)))
         END SELECT
           
         enerInternNew(km) = enerInternOld(km) + 
     +        stress_power/density(km)
           
         ! pwrinct Should be calculated somewhere if needed
         enerInelasNew(km) = enerInelasOld(km) + 
     +        pwrinct/density(km)
         
         !-----------------------------------------------------------
 
      END DO 
      ! End loop over material points
      
1000  FORMAT(15I8)
1100  FORMAT(20F8.3)  
     
      RETURN
      END SUBROUTINE vumatXtrArg
!*********************************************************************
      
!*********************************************************************
      SUBROUTINE network_stretch(F,elemnum)
! This subroutine calculates local stretch ratios of each fiber set in 
! one material point. Global variable gblsr1 is updated.
      
      USE ELEMGBL
      
      !--------------------------------------------------------------
      ! Declare variables
      IMPLICIT NONE
      
      REAL*8, PARAMETER :: pi=3.1415927
      
      INTEGER :: i,elemnum, fnum
      REAL*8 :: F(3,3),C(3,3),Nc(3),lambdac     
      REAL*8 :: nutheta,theta(numf)
      
      !--------------------------------------------------------------
      ! Initialize variables
      DO i = 1,numf
          theta(i) = 180./numf*(i-1)
      END DO

      !--------------------------------------------------------------
	! Loop over fibers aligned at different angles
      ! Calculate the local stretch ratios of each fiber set
	DO fnum = 1,numf
          
          ! Fiber stretch in this direction
          Nc(1) = COSD(theta(fnum))
          Nc(2) = SIND(theta(fnum))
          Nc(3) = 0.
          C = MATMUL(TRANSPOSE(F),F)
          lambdac = SQRT(DOT_PRODUCT(MATMUL(C,Nc),Nc))
          
          gblsr1(elemnum,fnum) = lambdac
              	
      END DO
      
      RETURN
      END SUBROUTINE network_stretch
!*********************************************************************

!*********************************************************************
      SUBROUTINE network_damage(elemnum)
! This subroutine updates damage information of each fiber set in one
! material point. State variables of current material point (statev) are updated
      
      USE MATPARAM
      USE ELEMGBL
      
      !--------------------------------------------------------------
      ! Declare variables
      IMPLICIT NONE
      
      INTEGER :: i,fnum,fnum_vert,elemnum
      REAL*8 :: F(3,3),C(3,3),Nc(3),lambdac  
      REAL*8 :: a1,a2,a3,a4,a5,a6
      REAL*8 :: D_trial
      
      !--------------------------------------------------------------
      ! Material parameters    
      a1 = matprop(9)
      a2 = matprop(10)
      a3 = matprop(11)
      a4 = matprop(12)
      a5 = matprop(13)
      a6 = matprop(14)

      !--------------------------------------------------------------
	! Loop over all the fibers aligned at different angles
	DO fnum = 1,numf
          
          ! Use non-local fiber stretch
          lambdac = gblsr2(elemnum,fnum)

          ! Trial state
          D_trial = 1.-1./3.*EXP(-((lambdac-1.)/a1)**a2)
     +                -1./3.*EXP(-((lambdac-1.)/a3)**a4)
     +                -1./3.*EXP(-((lambdac-1.)/a5)**a6)
          
          ! Update damage variable based on loading-unloading conditions
          IF ((D_trial .GE. statev(4,fnum)) .AND. 
     +        (lambdac .GE. 1.)) THEN
              
              statev(4,fnum) = D_trial
          END IF
              	
      END DO
      
      !--------------------------------------------------------------
      ! Fiber sets that are perpendicular to each other share one damage variable
      ! fnum_vert is the number of fiber that is perpendicular to the current fiber set
      DO fnum = 1,numf
          IF (fnum .LE. numf/2) THEN
              fnum_vert = numf/2+fnum
          ELSE
              fnum_vert = fnum-numf/2
          END IF
          
          statev(4,fnum) = MAX(statev(4,fnum),statev(4,fnum_vert))
      END DO

      RETURN
      END SUBROUTINE network_damage
!*********************************************************************

!*********************************************************************
      SUBROUTINE network_stress(F,T,delta_t,vf_def)
! This subroutine calculates fiber network stress response
      
      USE MATPARAM
      USE ELEMGBL
      
      !--------------------------------------------------------------
      ! Variable declaration
      IMPLICIT NONE
      
      REAL*8, PARAMETER :: pi=3.1415927
      
      INTEGER i,i2,j2,fnum
      REAL*8 :: F(3,3),DetF,C(3,3),Nc(3),Dyad_N(3,3),lambdac   
      REAL*8 :: nutheta,theta(numf),delta_t
      REAL*8 :: s0(numf),dmg(numf)
      REAL*8 :: T(3,3), PK2(3,3)
      REAL*8 :: vf_def    ! variable to scale volume fraction of a ``defect'' element

      !--------------------------------------------------------------
      ! Initialize variables
      nutheta = 1./numf
      DO i = 1,numf
          theta(i) = 180./numf*(i-1)
      END DO
      PK2 = 0.
      dmg = 0.

      CALL MDET(F,DetF)
      
      !--------------------------------------------------------------
	! Loop over fibers aligned at different angles
	DO fnum = 1,numf
          
          ! Fiber stretch in this direction
          Nc(1) = COSD(theta(fnum))
          Nc(2) = SIND(theta(fnum))
          Nc(3) = 0.
          C = MATMUL(TRANSPOSE(F),F)
          lambdac = SQRT(DOT_PRODUCT(MATMUL(C,Nc),Nc))
          
              
          ! Calculate fiber stresses and update state variables
          ! Here, s0 is fiber stress
          CALL sub_fiber_elpl(lambdac,delta_t,statev(1:3,fnum),
     +                                statev(1:3,fnum),s0(fnum))

          ! Damage variable of current fiber
          dmg(fnum) = statev(4,fnum)

          ! Calculate network stress
          DO i2  = 1, 3 
              DO j2 = 1, 3
                  Dyad_N(i2,j2) = Nc(i2)*Nc(j2)
              END DO
          END DO	
          PK2 = PK2 + (1.- dmg(fnum))*matprop(1)*nutheta*
     +                    (s0(fnum)/lambdac)*Dyad_N*vf_def
              	
      END DO
      
      ! Cauchy stress response
      T = MATMUL(MATMUL(F,PK2),TRANSPOSE(F))/DetF

      RETURN
      END SUBROUTINE network_stress
!********************************************************************* 
    
!*********************************************************************
      SUBROUTINE sub_fiber_elpl(lambda,dt,statev_old,statev_new,
     +                          fstress)
! This subroutine is a 1-D viscoplastic model for fibers
! Output (fstress) is fiber nominal stress
      
      ! Declare variables
      USE MATPARAM
      IMPLICIT NONE
      REAL*8 :: lambda,dt,statev_old(3),statev_new(3),fstress
      REAL*8 :: Ef,e0_dot,s0,n,h,ssat
      REAL*8 :: lambda_e,gamma_p,D_p,lambda_p_dot
      REAL*8 :: cauchy,s,s1,s2,s1_dot,s2_dot
    
      !----------------------------------------------------
      ! Material parameters
      Ef = matprop(3)
      e0_dot = matprop(4)
      s0 = matprop(5)
      n = matprop(6)
      h = matprop(7)
      ssat = matprop(8)
      
      !----------------------------------------------------
      ! Current states
      lambda_e = lambda/statev_old(1)
      cauchy = Ef*lambda_e*(lambda_e-1.)
      fstress = cauchy/lambda
      s = statev_old(2)+statev_old(3)
      
      ! Flow rule
      gamma_p = e0_dot*SINH(ABS(cauchy)/s)
      D_p = SIGN(gamma_p,cauchy)
      
      ! Update plasticity state variables
      ! Hardening rule is applied
      lambda_p_dot = D_p*statev_old(1)
      s1_dot = s0*(statev_old(1)**n)*gamma_p
      s2_dot = h*(1-statev_old(3)/ssat)*gamma_p
      statev_new(1) = statev_old(1)+lambda_p_dot*dt
      statev_new(2) = statev_old(2)+s1_dot*dt
      statev_new(3) = statev_old(3)+s2_dot*dt

      RETURN
      END SUBROUTINE sub_fiber_elpl
!*********************************************************************
      
!*********************************************************************
      SUBROUTINE seam_geo(p1,p2,crt)
! This suboutine checks whether the line between two elements passes a seam
      
      ! Declare variables
      IMPLICIT NONE
      REAL*8 :: p1(2),p2(2),seamx,seamy1,seamy2
      REAL*8 :: crt1,crt2,crt
      
      ! Seam location
      seamx = 20.
      seamy1 = 24.
      seamy2 = 56.
      
      crt = 0.
      ! Criteria of whether the line between two elements passes the seam
      crt1 = (p1(1)-seamx)*(p2(1)-seamx)
      crt2 = ((p1(1)*p2(2)-p2(1)*p1(2)) - (p2(2)-p1(2))*seamx) /
     +        (p1(1)-p2(1))
      IF ((crt1 . LE. 0.) .AND. (crt2 .GE. seamy1) .AND. 
     +    (crt2 .LE. seamy2)) THEN
          crt = 1.
      END IF
      
      RETURN
      END SUBROUTINE seam_geo
!*********************************************************************  

!*********************************************************************
      INCLUDE 'UR_shortlist.for'  
!*********************************************************************
      
