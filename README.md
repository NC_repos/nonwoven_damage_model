# nonwoven_damage_model

## Description
A constitutive model for non-woven fiber network materials. Users can define damage evolution parameters and non-local interaction distance in the Abaqus user subroutines included.

## Running the model
Submit a job:
```
abaqus double job=example_coupon user=vumat_nonwoven_damage_coupon.for inter
```
or 
```
abaqus double job=example_crack user=vumat_nonwoven_damage_crack.for inter
```

Calibrate damage evolution parameters:
```
./bond_model_fit/main.m
```


## Reference
@article{chen2019micromechanics,
  title={A micromechanics-based damage model for non-woven fiber networks},
  author={Chen, Naigeng and Silberstein, Meredith N},
  journal={International Journal of Solids and Structures},
  volume={160},
  pages={18--31},
  year={2019},
  publisher={Elsevier}
}

