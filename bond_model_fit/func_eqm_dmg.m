
function eqm = func_eqm_dmg(x,mat,statev_old,lambda_new,dt,s0)
    
    % Visco-plastic fibers
    [s1,~] = func_fiber_viscopl(mat,statev_old(4:6),x(1),dt);
    [s2,~] = func_fiber_viscopl(mat,statev_old(7:9),x(2),dt);
    
    % Damaged state
    nlP = 1/(1-x(3))^mat.nlb*mat.nlP;
    En = (1-x(3))*mat.En;
    
    % Bond strength at current step
    bondforce = wblinv(x(3),mat.bwblA,mat.bwblB);
    bondstrength = bondforce/(pi/4*mat.fiberd^2);
    
    % Equilibrium residual
    eqm(1) = s1 + nlP*s1^3/mat.Ef^2 - s2;
    eqm(2) = s2 - (1-x(3))*s0 - En*mat.eta*...
            (2*lambda_new-x(1)-x(2));
    eqm(3) = (s2-s1) - bondstrength;
    
end