
function [eng_stress,statev] = func_fiber_viscopl(mat,statev,lambda,dt)
        
        % Current states
        % Elastic stretch, Cauchy and engineering stress, shear resistance
        lambda_e = lambda/statev(1);
        cauchy = mat.Ef*lambda_e*(lambda_e-1);
        eng_stress = cauchy/lambda;
        s_old = statev(2)+statev(3);
        
        % Flow rule
        gamma_p = mat.e0_dot * sinh(abs(cauchy)/s_old);
        D_p = gamma_p*sign(cauchy);

        % Update state variables
        % Hardening rule is applied
        lambda_p_dot = D_p*statev(1);
        s1_dot = mat.s0*(statev(1)^mat.n)*gamma_p;
        s2_dot = mat.h*(1-statev(3)/mat.ssat)*gamma_p;
        statev(1) = statev(1) + lambda_p_dot*dt;
        statev(2) = statev(2) + s1_dot*dt;
        statev(3) = statev(3) + s2_dot*dt;

        
end