
function eqm = func_eqm_trial(x,mat,statev_old,lambda_new,dt,s0)
    
    % Visco-plastic fibers
    [s1,~] = func_fiber_viscopl(mat,statev_old(4:6),x(1),dt);
    [s2,~] = func_fiber_viscopl(mat,statev_old(7:9),x(2),dt);
    
    % Damaged state
    nlP = 1/(1-statev_old(10))^mat.nlb*mat.nlP;
    En = (1-statev_old(10))*mat.En;
    
    % Equilibrium residual
    eqm(1) = s1 + nlP*s1^3/mat.Ef^2 - s2;
    eqm(2) = s2 - (1-statev_old(10))*s0 - En*mat.eta*...
            (2*lambda_new-x(1)-x(2));


end