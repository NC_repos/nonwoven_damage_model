
%% 
% This program fits single bond damage model parameters

close all
clear all
clc

%% MATERIAL INPUTS

% Material parameters
net.Ef = 2400;
net.e0_dot = 1e-18;
net.s0 = 1.7336;
net.n = 4.9603;
net.h = 41.5508;
net.ssat = 10.6770;


% SF20
net.nlP = 30;
net.nlb = 0.1;
net.fiberd = 0.05;
net.eta = 23.14;
net.En = 164;

% % SF32
% net.nlP = 8;
% net.nlb = 1;
% net.fiberd = 0.05;
% net.eta = 25.34;
% net.En = 216;

% % SF65
% net.nlP = 4;
% net.nlb = 1.6;
% net.fiberd = 0.05;
% net.eta = 25.89;
% net.En = 315;


% Bond strength (unit: newton)
% Weibull bond strength
net.bwblA = 0.0778209;
net.bwblB = 1.14289;


%% LOADING SPECIFICATIONS

% lambda_step: the transition point
% speed: load speed
numpt = 800;
load_step = [1,1.8];
speed = [0.1];

% Initialize lambda and dt
% lambda: all the incremental step
% dt: all the time increments
lambda = [1];
dt = [0];
for step = 1:(numel(load_step)-1)
    lambda_step = linspace(load_step(step),load_step(step+1),numpt);
    time_increment = (lambda_step-[0,lambda_step(1:end-1)])./speed(step);
    
    lambda = [lambda,lambda_step(2:end)];
    dt = [dt,time_increment(2:end)];
    
end   

%% STATE VARIABLES

% State variables (plasticity and damage)
% Overall fiber (fiber0) lambda_p, shear resistance s_1, s_2
% fiber1 lambda_p, fiber1 shear resistance s_1, s_2
% fiber2 lambda_p, fiber2 shear resistance s_1, s_2
% damage variable
statev = [1,net.s0,0,1,net.s0,0,1,net.s0,0,0];
statev_new = [1,net.s0,0,1,net.s0,0,1,net.s0,0,0];

%% LOADING STEPS

% Initialize local force balance variables
s0 = zeros(1,numel(lambda));    % Overall fiber stress
s1 = zeros(1,numel(lambda));    % Fiber 1 stress
s2 = zeros(1,numel(lambda));    % Fiber 2 stress
s3 = zeros(1,numel(lambda));    % Nonlinear term
lambda1 = ones(1,numel(lambda));
lambda2 = ones(1,numel(lambda));

dmg = zeros(1,numel(lambda));
phi_d = zeros(1,numel(lambda));
cauchy = zeros(1,numel(lambda));

% Check equilibrium state
check_eqm = zeros(2,numel(lambda));

% Loop over all the load, unload and reload steps
for i = 2:numel(lambda)
    
    [s0(i),statev_new(1:3)] = func_fiber_viscopl(net,statev(1:3),lambda(i),dt(i));
    
    
    % ---------------------------------------------------------
    % Trial states
    
    % Bond strength at current step
    bondforce = wblinv(statev(10),net.bwblA,net.bwblB);
    bondstrength = bondforce/(pi/4*net.fiberd^2);
    
    % Solve for the equilibrium state of the bond structure
    fun1 = @(x) func_eqm_trial(x,net,statev,lambda(i),dt(i),s0(i));
    lambda0 = [lambda(i),lambda(i)];
    options = optimoptions('fsolve','display','off');
    lambda_result = fsolve(fun1,lambda0,options);
    lambda1_trial = lambda_result(1);
    lambda2_trial = lambda_result(2);
    
    
    % Trial fiber stresses
    [s1_trial,statev_new(4:6)] = func_fiber_viscopl(net,statev(4:6),lambda1_trial,dt(i));
    [s2_trial,statev_new(7:9)] = func_fiber_viscopl(net,statev(7:9),lambda2_trial,dt(i));
    
    % Activation function
    phi_d_trial = (s2_trial-s1_trial) - bondstrength;
    
    % ---------------------------------------------------------
    % Damage criteria and evolution
    if phi_d_trial<0
        % No damage evolution
        s1(i) = s1_trial;
        s2(i) = s2_trial;
        lambda1(i) = lambda1_trial;
        lambda2(i) = lambda2_trial;
        dmg(i) = statev_new(10);
        phi_d(i) = phi_d_trial;
        s3(i) = 1/(1-dmg(i))^net.nlb*net.nlP*...
                s1(i)^3/net.Ef^2;
        
        % Check equilibrium states
        check_eqm(:,i) = func_eqm_trial(lambda_result,net,statev,lambda(i),dt(i),s0(i));
        
        % Update state variables
        statev = statev_new;
        
    else

        % Solve for damage states
        fun2 = @(x) func_eqm_dmg(x,net,statev,lambda(i),dt(i),s0(i));
        x0 = [lambda1(i-1),lambda2(i-1),statev(10)];
        options = optimoptions('fsolve','display','off');
        x_result = fsolve(fun2,x0,options);
        lambda1(i) = x_result(1);
        lambda2(i) = x_result(2);
        statev_new(10) = x_result(3);
        
        % Fiber stresses
        % Trial fiber stresses
        [s1(i),statev_new(4:6)] = func_fiber_viscopl(net,statev(4:6),lambda1(i),dt(i));
        [s2(i),statev_new(7:9)] = func_fiber_viscopl(net,statev(7:9),lambda2(i),dt(i));
        dmg(i) = statev_new(10);
        s3(i) = 1/(1-dmg(i))^net.nlb*net.nlP*...
                s1(i)^3/net.Ef^2;
        
        % Check equilibrium states
        temp_eqm = func_eqm_dmg(x_result,net,statev,lambda(i),dt(i),s0(i));
        check_eqm(:,i) = [temp_eqm(1);temp_eqm(2)];
        phi_d(i) = temp_eqm(3);

        % Update state variables
        statev = statev_new;
        
    end
    
    % ---------------------------------------------------------
    % Overall stress
    % One need to convert engineering stress to cauchy stress
    cauchy(i) = (1-dmg(i))*s0(i)*lambda(i);
    
    h = waitbar(i/numel(lambda));
end

close(h);

output.lambda = lambda;
output.cauchy = cauchy;
output.dmg = dmg;



%% PLOTTING

figure(1)
subplot(2,2,1);
plot(lambda,s1,'--b','linewidth',1.5); hold on
plot(lambda,s2,'--r','linewidth',1.5); hold on
plot(lambda,s3,'--k','linewidth',1.5); hold on
plot(lambda,(1-dmg).*s0,'-b','linewidth',1.5); hold on
plot(lambda,(1-dmg).*(s1+s2)./2,'-g','linewidth',1.5);
legend('Fiber1','Fiber2','Nonlinear term','RVE fiber','Average of fiber 1&2');
% set(legend,'fontsize',18)
xlabel('Far field stretch');
ylabel('Stress');

subplot(2,2,2);
plot(lambda,lambda1,'--b','linewidth',1.5); hold on
plot(lambda,lambda2,'--r','linewidth',1.5); hold on
plot(lambda,lambda,'-b','linewidth',1.5); hold on
legend('Fiber1','Fiber2','Far field stretch');
% set(legend,'fontsize',18)
xlabel('Far field stretch');
ylabel('Stretch ratio');

subplot(2,2,3)
plot(lambda,check_eqm(1,:),'-b'); hold on
plot(lambda,check_eqm(2,:),'-r');
xlabel('Far field stretch');
ylabel('Equilibrium residual');

subplot(2,2,4);
yyaxis left
plot(lambda,phi_d);
xlabel('Strain');
ylabel('\phi_d');
yyaxis right
plot(lambda,dmg);
ylabel('Damage variable D');


% return

%% FITTING PROCESS

x0 = [0.17, 1.8, 0.31, 1.9, 1.1, 1.2];

xdata = lambda;
ydata = dmg;

% func_fit = @(x,xdata) x(1)-x(1)*exp(-((xdata-1)./x(2)).^x(3))+x(4).*(xdata-1);
func_fit = @(x,xdata) 1-(1/3).*exp(-((xdata-1)./x(1)).^x(2))...
                       -(1/3).*exp(-((xdata-1)./x(3)).^x(4))...
                       -(1/3).*exp(-((xdata-1)./x(5)).^x(6));
options = optimoptions('lsqcurvefit','MaxFunctionEvaluations',6000,'MaxIteration',2000);

x = lsqcurvefit(func_fit,x0,xdata,ydata,[],[],options)

figure()
plot(lambda,dmg,'.b'); hold on
plot(lambda,func_fit(x,lambda),'-r'); hold on
%plot(lambda,func_fit(x0,lambda),'-k'); hold on